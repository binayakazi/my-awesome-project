'use client'

import React, { useEffect, useState } from 'react'
import { useMediaQuery } from 'react-responsive';

export const Nav = () => {

	const [isOpen, setIsOpen] = useState(true);

	const isMobile = useMediaQuery({ query: '(max-width: 767px)' });

	useEffect(() => {
		
	})

	const toggle = () => {
		if(isMobile) {
			setIsOpen(!isOpen)
		}
		setIsOpen(true);
	};

	return (
		<>
			<nav className='pb-4 md:pb-0' style={{ background: '#333', color: '#fff' }}>
				<div className='2xl:container px-4'
					style={{
						display: 'grid',
						gridTemplateColumns: '1fr auto',
					}}>
					<div className=''>
						<div>Logo</div>
					</div>
					<div
						style={{ display: isOpen ? 'flex' : 'none'}}
						className='md:flex-row md:space-y-0 md:space-x-4 md:py-2 flex flex-col justify-center items-center space-y-2 order-2'>
						<div className=''>
							<a href="" className=''>Home</a>
						</div>
						<div className=''>
							<a href="" className=''>About</a>
						</div>
						<div className=''>
							<a href="" className=''>Project</a>
						</div>
						<div className=''>
							<a href="" className=''>Contact</a>
						</div>
					</div>
					<div className='md:hidden'>
						<input onClick={toggle} type="checkbox" name="" id="" />
					</div>
				</div>
			</nav>
		</>
	)
}