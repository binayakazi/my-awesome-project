import { pageContainer } from "@/app/preset-style"


const nav = {
    background: '#424243',
    color: '#fff',
}

const navLinks = {
    ...pageContainer
}

const logo = {
    alignItems: 'center' as 'center'
}

export {
    nav,
    navLinks,
    logo
}